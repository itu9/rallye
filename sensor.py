from signal import signal, SIGTERM, SIGHUP, pause
import RPi.GPIO as GPIO
from time import sleep
from threading import Thread
from gpiozero import DistanceSensor

reading = True
devant = DistanceSensor(echo=16, trigger=25)
gauche = DistanceSensor(echo=20, trigger=24)
droite = DistanceSensor(echo=21, trigger=23)
autoPilote = 0
guide = 50

class Capteur:
    def __init__(self, echo, trig, minDist):
        self.echo = echo
        self.trig = trig
        self.minDist = minDist
        self.value = 0
        self.servo = DistanceSensor(echo=self.echo,trig=self.trig)
        
    def directValue(self):
        result = self.servo.value
        sleep(0.1)
        return result
        
    def refresh(self):
        self.value = self.directValue()
        
    def getValue(self):
        return self.value 
    
    def safe(self):
        return self.getValue() < self.minDist
    
class Pneu:
    def __init__(self,en:int,rouge:int,noire:int,sortie:int):
        self.en = en
        self.rouge = rouge
        self.noire = noire
        self.sortie = sortie
        self.initialiser()
        
    def initialiser(self):
        GPIO.setup(self.rouge,GPIO.OUT)
        GPIO.setup(self.noire,GPIO.OUT)
        GPIO.setup(self.en,GPIO.OUT)
        self.pwm = GPIO.PWM(self.sortie)
    
    def demarer(self):
        pass
    
    def accelerer(self):
        pass
    
    def arreter(self):
        pass
    
class Voiture:
    def __init__(self, devant:Capteur, gauche:Capteur, droite:Capteur,pGauche:Pneu, pDroite:Pneu):
        self.devant:Capteur = devant
        self.gauche:Capteur = gauche
        self.droite:Capteur = droite
        self.pGauche:Pneu = pGauche
        self.pDroite:Pneu = pDroite
        self.etat = guide
        self.temps360 = 3
        
    def isAutoPilote(self):
        return self.etat == autoPilote
    
    def avancer(self):
        self.pGauche.demarer()
        self.pDroite.demarer()
        
    def arreter(self):
        self.pGauche.arreter()
        self.pDroite.arreter()
    
    def gauche(self):
        self.arreter()
        self.pGauche.demarer()
        
    def droite(self):
        self.arreter()
        self.pDroite.demarer()
        
    def matchTime(self, angle):
        rapport = 360./angle
        return self.temps360/rapport
        
    def temps180(self):
        return self.matchTime(180) 
    
    def temps90(self):
        return self.matchTime(90) 
        
    def gaucheDemi(self):
        self.droite()
        sleep(self.temps90())
        self.arreter()
        
    def droiteDemi(self):
        self.gauche()
        sleep(self.temps90())
        self.arreter()
        
    def demiTour(self):
        self.gauche()
        sleep(self.temps180())
        self.arreter()
        
    def tourner(self, angle):
        self.gauche()
        sleep(self.matchTime(angle))
        self.arreter()
        
    def verifDevant(self):
        if self.devant.safe():
            self.avancer()
        else:
            if self.gauche.safe():
                self.gaucheDemi()
            elif self.droite.safe():
                self.droiteDemi()
            else:
                self.demiTour()
                
    def verifGauche(self):
        if not self.gauche.safe():
            self.pGauche.accelerer()
            
    
    def verifDroite(self):
        if not self.droite.safe():
            self.pDroite.accelerer()
    
    def autoPilote(self): 
        self.devant.refresh()
        self.gauche.refresh()
        self.droite.refresh()
        self.verifDevant()
        self.verifGauche()
        self.verifDroite()
        
    def guider(self):
        pass
        
    def run(self):
        while True:
            if self.isAutoPilote():
                self.autoPilote()
            else :
                self.guider()
    

devantValue = 0
devantMin = 0.2
gaucheValue = 0
gaucheMin = 0.1
droiteValue = 0
droiteMin = 0.1

def safe_exit(signum, frame):
    exit(1)
    
def avancer():
    print("avancer")
    
def gaucheComplet():
    print("gauche")
    
def droiteComplet():
    print("droite")
    
def tourComplet():
    print("complet")
    
def read():
    while reading:
        devantValue = devant.value
        sleep(0.1)
        gaucheValue = gauche.value
        sleep(0.1)
        droiteValue = droite.value
        sleep(0.1)
        
def safeLeft() :
    return gaucheValue < gaucheMin

def safeRight():
    return droiteValue < droiteMin

def safeUp() :
    return devantValue < devantMin

try:
    signal(SIGTERM, safe_exit)
    signal(SIGHUP, safe_exit)

    reader = Thread(target=read, daemon=True)
    reader.start()

    pause()

except KeyboardInterrupt:
    pass

finally:
    reading = False
    reader.join()
    devant.close()